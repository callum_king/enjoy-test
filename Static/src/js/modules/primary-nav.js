﻿
APP.siteNavigation = (function($) {
  'use strict';
  
  var element = {
    header: '#js-header',
    headerLogo: '#js-header-logo',
    headerActions: '#js-header-actions',
    navIconToggle: '#js-nav-toggle',
    menu: '#js-primary-nav',
    primaryNavTier1Link: '.c-primary-nav__tier1-link',
    primaryNavTier2Link: '.c-primary-nav__tier2-link',
    primaryNavTier1Item: '.c-primary-nav__tier1-item',
    primaryNavTier2Item: '.c-primary-nav__tier2-item'
  };

  var init = function() {

    enquire.register("screen and (max-width: 768px)", {
      match : function() {
        createMobileSiteNavigation();
        killMenuHoverHandler();
      },
      unmatch : function() {
        clearMobileSiteNavigation();
        createSiteNavigation();
      }
    });
  };

  // Create functionality for desktop site navigation
  var createSiteNavigation = function() {

    createMenuHoverHandler(element.primaryNavTier1Item);
    createMenuHoverHandler(element.primaryNavTier2Item);

  };

  // Create functionality for mobile site navigation
  var createMobileSiteNavigation = function() {

    if ($(element.menu).length) {
      createEventHandlers();
    }
  };

  // Clear 'leftover' classes from mobile site navigation
  var clearMobileSiteNavigation = function() {

    if ($('.-nav-is-active')){
      $.each( $('.-nav-is-active'), function(){ 
        $(this).removeClass('-nav-is-active');
      }); 
    }

    if ($('.-link-is-active')){
      $.each( $('.-link-is-active'), function(){ 
        $(this).removeClass('-link-is-active');
      }); 
    }
  };

  // Create event handlers
  var createEventHandlers = function(){

    // On navIconToggle click,
    // - Add '-is-open' class to self
    // - Add '-primary-nav-is-active' class to header & header actions
    $(element.navIconToggle).off('click').on('click', function(e){
      e.preventDefault();

      $(this).toggleClass('-is-open');
      
      $(element.header).toggleClass('-primary-nav-is-active');
      $(element.headerActions).toggleClass('-primary-nav-is-active');
    });

    createTierClickHandler(element.primaryNavTier1Link);
    createTierClickHandler(element.primaryNavTier2Link);
  };

  // Create tier click functionality for mobile navigation
  //
  // If
  var createTierClickHandler = function(element){

    // On element click
    $(element).click(function(evt){

      // Get current tier class
      var currentTierClass = evt.target.classList[0];

      // If this tier class does not have class '-link-is-active', proceed
      if (!$(this).hasClass('-link-is-active') ){

        // Prevent hyperlink working
        // Hyperlink will only work once link has been clicked once and assigned class -link-is-active
        evt.preventDefault();

        // For each item of current tier
        $.each( $(currentTierClass), function(e){

          // If this item has -link-is-active class
          // - Remove class
          if ($(this).hasClass('-link-is-active')){
            $(this).removeClass('-link-is-active');

            // If this parent has active navigation class
            // - Remove class
            if( $(this).parent().hasClass('-nav-is-active') ){
              $(this).parent().removeClass('-nav-is-active');
            }
          }

        });

        // Add active link class to current item
        $(this).addClass('-link-is-active');
        // Add active nav class to current item parent
        $(this).parent().addClass('-nav-is-active');
        
      }
    });
  }

  // Create menu hover handler functionality [unfinished/in progress]
  var createMenuHoverHandler = function(element){

    // On element mouseenter, add '-is-hovered' class
    $(element).on('mouseenter', function(){
      $(this).addClass('-is-hovered');
    });

    $(element).on('mouseleave', function(){
      $(this).removeClass('-is-hovered');
    });
  }

  // Kill menu hover handler functionality
  var killMenuHoverHandler = function(){
    $(element.primaryNavTier1Item).off('mouseenter mouseleave');
    $(element.primaryNavTier2Item).off('mouseenter mouseleave');
  }

  return {
    init: init
  };

})(jQuery);