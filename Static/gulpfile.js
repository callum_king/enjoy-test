// Load plugins
var gulp = require('gulp');
var watch = require('gulp-watch');
var batch = require('gulp-batch');
var del = require('del');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var filter = require('gulp-filter');
var autoprefixer = require('gulp-autoprefixer');
var include = require('gulp-file-include');
var sprite = require('gulp-svg-sprite');
var svg2png = require('gulp-svg2png');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var jshint = require('gulp-jshint');
var runSequence = require('run-sequence');
var concat = require('gulp-concat');
var pngquant = require('imagemin-pngquant');
var optipng = require('imagemin-optipng');
var svgo = require('imagemin-svgo');
var mozjpeg = require('imagemin-mozjpeg');

var modernizr = require('gulp-modernizr');
var modRewrite = require('connect-modrewrite');


// Build a paths object
var paths = (function () {
  var obj = {};

  obj.root = './';
  obj.src = obj.root + 'src/';
  obj.build = obj.root + 'build/';

  obj.templates = obj.root + 'templates/';

  obj.sass = obj.src + 'sass/';
  obj.css = obj.build + 'css/';

  //obj.srcCSSLibs = obj.src + 'css/libs/';
  //obj.buildCSSLibs = obj.build + 'css/libs/';

  obj.srcJS = obj.src + 'js/';
  obj.buildJS = obj.build + 'js/';
  obj.srcJSLibs = obj.srcJS + 'libs/';

  obj.images = obj.root + 'images/';
  obj.svg = obj.images + 'svg/';
  obj.sprites = obj.images + 'sprites/';

  return obj;
})();

gulp.task('modernizr', function() {
  gulp.src([
    paths.srcJS + 'main.js',
    paths.srcJS + 'modules/*.js',
    paths.sass + '**/*.scss'
  ])
  .pipe(modernizr({'options': ['setClasses', 'addTest', 'html5printshiv', 'testProp', 'fnBind']}))
  .pipe(uglify())
  .pipe(gulp.dest(paths.buildJS));
});


// Pipe HTML through BrowserSync
gulp.task('html', function () {
  return gulp.src(paths.templates + 'pages/*.html')
  .pipe(include({
    prefix: '@@',
    basepath: '@file'
  }))
  .pipe(gulp.dest(paths.root))
  .pipe(browserSync.reload({ stream:true }));
});


// Compile Sass & pipe through BrowserSync
gulp.task('sass', ['cleanCSS'], function() {
  return gulp.src(paths.sass + '*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(filter('**/*.css'))
  .pipe(autoprefixer({
    browsers: ['last 2 versions', 'ie >= 9']
  }))
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest(paths.css))
  .pipe(browserSync.reload({ stream:true }));
});


// Clean CSS
gulp.task('cleanCSS', function() {
  return del(paths.css);
});


// Clean build folder
gulp.task('clean', function() {
  return del(paths.build);
});


gulp.task('serve', function() {
  var port = 8000;

  browserSync({
    server: {
      baseDir: paths.root,
      port: port,
      middleware: [
         modRewrite([
            '^/Static/(.*)$ http://localhost:' + port + '/$1 [P]'
         ])
      ]
    },
    port: port,
    tunnel: false
  });
});


// Create SVG sprite assets with PNG backup
gulp.task('sprites', function () {
  var config = {
    shape: {
      spacing: {
        padding: 20
      },
    },

    mode: {
      css: {
        layout: 'horizontal',
        sprite: '../sprite.svg',
        bust: false,
        render: {
          scss: {
            template: paths.sprites + '.sprite-template',
            dest: '../../../src/sass/utilities/_sprites.scss'
          }
        }
      },
    },

    variables: {
      //svgPath: '../../images/sprites/sprite.svg',
      //pngPath: '../../images/sprites/sprite.png'
      svgPath: '/Static/images/sprites/sprite.svg',
      pngPath: '/Static/images/sprites/sprite.png'
    }
  };

  return gulp.src(paths.svg + '**/*.svg')
  .pipe(sprite(config))
  .pipe(gulp.dest(paths.sprites));
  //.pipe(filter('**/*.svg'))
  //.pipe(svg2png())
  //.pipe(rename({extname: '.png'}))
  //.pipe(gulp.dest(paths.sprites));
});


// Optimize images
gulp.task('optimizeImg', function () {
  return gulp.src(paths.images + '**/*.{png,jpg,jpeg,gif,svg}')
  .pipe(pngquant({quality: '65-80', speed: 4})())
  .pipe(optipng({optimizationLevel: 3})())
  .pipe(mozjpeg({quality: 70})())
  .pipe(svgo()())
  .pipe(gulp.dest(paths.images))
  .pipe(browserSync.reload({ stream:true }));
});


// Lint JS & pipe through BrowserSync
gulp.task('lint', function() {
  return gulp.src(['gulpfile.js', paths.srcJS + '**/*.js', '!' + paths.srcJSLibs + '*.js', '!' + paths.srcJS + 'polyfills/*.js'])
  .pipe(jshint())
  .pipe(jshint.reporter('default'))
  .pipe(browserSync.reload({ stream:true }));
});


// Copy JS
gulp.task('copyJS', function() {
  var dest = paths.buildJS;

  return gulp.src([
    paths.srcJSLibs + 'jquery-1.11.3.min.js',
    paths.srcJS + 'polyfills/*.js'
  ])
  .on('success', function(succ) {
    console.log('\033[92mJavascript copied successfully!\033[39m');
  })
  .on('error', function(err) {
    console.log('\033[91m' + err + '\033[39m');
    this.emit('end');
  })
  .pipe(gulp.dest(dest));
});


// Concat JS
gulp.task('concatJS', function() {
  var dest = paths.buildJS;

  return gulp.src([
    paths.srcJS + 'main.js',
    paths.srcJS + 'modules/*.js'
  ])
  .pipe(concat('scripts.js'))
  .pipe(gulp.dest(dest));
});


// Concat JS Libs
gulp.task('concatJSLibs', function() {
  var dest = paths.buildJS;

  return gulp.src([
    paths.srcJSLibs + 'jquery.cookie.min.js',
    paths.srcJSLibs + 'jquery.cycle2.js',
    paths.srcJSLibs + 'jquery.matchHeight-min.js',
    paths.srcJSLibs + 'modal.js',
    paths.srcJSLibs + 'transition.js',
    paths.srcJSLibs + 'jquery.fitvids.js',
    paths.srcJSLibs + 'fastclick.js',
    paths.srcJSLibs + 'enquire.js',
    paths.srcJSLibs + 'yepnope.js'
  ])
  .pipe(concat('libs.js'))
  .pipe(gulp.dest(dest));
});


// Minify CSS
gulp.task('minifyCSS', function() {
  return gulp.src(paths.css + '**/*.css')
  .pipe(cssnano({keepSpecialComments:0}))
  .pipe(gulp.dest(paths.css));
});


// Minify JS
gulp.task('minifyJS', function() {
  return gulp.src([paths.buildJS + '*.js'])
  .pipe(uglify({
    compress: {
      drop_console: true
    }
  }))
  .pipe(gulp.dest(paths.buildJS));
});


// Watch files for changes
// * uses the gulp-watch npm package because standard
// gulp.watch doesn't track new or deleted files for changes

gulp.task('watchAll', function() {
  watch(paths.templates + '**/*.html', batch(function (events, done) {
    gulp.start('html', done);
  }));

  watch(paths.sass + '**/*.scss', batch(function (events, done) {
    gulp.start('sass', done);
  }));

  watch(paths.svg + '**/*.svg', batch(function (events, done) {
    gulp.start('sprites', done);
  }));

  watch(['gulpfile.js', paths.srcJS + '**/*.js', '!' + paths.srcJSLibs + '*.js'], batch(function (events, done) {
    gulp.start(['lint', 'copyJS', 'concatJS'], done);
  }));

  watch(paths.srcJSLibs + '*.js', batch(function (events, done) {
    gulp.start('concatJSLibs', done);
  }));
});


// A task that copies assets into the build folder
gulp.task('copyAll', ['copyJS', 'concatJS', 'concatJSLibs']);

// A pre build task that is used for both dev and deploy
gulp.task('build', function(callback) {
  runSequence(['clean','lint', 'sprites'], ['html', 'sass', 'copyAll'], 'modernizr', callback);
});

//----------------------------------------------------------------------

// Default task
gulp.task('default', function(callback) {
  runSequence('build', 'serve', 'watchAll', callback);
});

// Deployment task
gulp.task('deploy', function(callback) {
  runSequence('build',['minifyCSS', 'minifyJS', 'optimizeImg'], callback);
});